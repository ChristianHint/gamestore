package gamestore.config;

import gamestore.model.entity.Event;
import gamestore.model.entity.PasswordResetToken;
import gamestore.service.EventService;
import gamestore.service.PasswordResetTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Component
public class Scheduler {
    private final EventService eventService;
    private final PasswordResetTokenService passwordResetTokenService;

    @Autowired
    public Scheduler(EventService eventService, PasswordResetTokenService passwordResetTokenService) {
        this.eventService = eventService;
        this.passwordResetTokenService = passwordResetTokenService;
    }

    //fixedRate = 1800000 - 30 mins
    //fixedRate = 60000 - 1 min
    //fixedRate = 300000 - 5 mins
    @Scheduled(fixedRate = 1800000)
    public void checkEventsValidity(){
        List<Event> events = this.eventService.findAllEvents();
        LocalDateTime localDateTime = LocalDateTime.now();

        for (Event e: events) {
            if(e.getEndedDate().isBefore(localDateTime)){
                e.setEventExpired(true);
                this.eventService.updateExpiredEvent(e);
            }
        }
    }

    @Scheduled(fixedRate = 1800000)
    public void checkTokenValidity(){
        List<PasswordResetToken> passwordResetTokenList = this.passwordResetTokenService.findAllTokens();
        Date date = new Date();

        for (PasswordResetToken p : passwordResetTokenList){
            if(p.getExpiryDate().before(date)){
                this.passwordResetTokenService.deleteToken(p);
            }
        }
    }
}

package gamestore.model.Details;

import gamestore.model.service.UserServiceModel;
import gamestore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    public UserDetailsServiceImpl() {
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserServiceModel user = this.userService.findByUsername(username);

        if(user != null && user.isEnabled()){
            return new MyUserDetails(user);
        }else{
            throw new UsernameNotFoundException("Could not find user");
        }

    }

}
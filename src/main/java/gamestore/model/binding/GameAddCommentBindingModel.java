package gamestore.model.binding;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class GameAddCommentBindingModel {
    private int score;
    private String textContent;

    public GameAddCommentBindingModel() {
    }

    @Min(value = 1, message = "Score must be between 1 and 10 inclusive!")
    @Max(value = 10, message = "Score must be between 1 and 10 inclusive!")
    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Size(min = 10, max = 10000, message = "Description length must be between 10 and 10000 characters!")
    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }
}

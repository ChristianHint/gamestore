package gamestore.model.binding;

import gamestore.model.entity.Category;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class GameChangeBindingModel {
    private String name;
    private String description;
    private String platform;
    private String requirements;
    private Category category;
    private BigDecimal price;

    public GameChangeBindingModel() {

    }

    @Length(min = 3, max = 30, message = "Name length must be between 3 and 30 characters!")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Size(min = 10, max = 1000, message = "Description length must be between 10 and 1000 characters!")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Size(min = 5, max = 250, message = "Platform length must be between 5 and 100 characters!")
    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Size(min = 10, max = 1000, message = "Requirements length must be between 10 and 1000 characters!")
    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    @NotNull
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @NotNull
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

}

package gamestore.model.binding;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class GameSaleBindingModel {
    private BigDecimal salePrice;

    public GameSaleBindingModel() {
    }

    @NotNull
    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }
}

package gamestore.model.binding;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class OrderAddBindingModel {
    private String name;
    private String billingAddress;
    private String phoneNumber;
    private String additionalInformation;

    public OrderAddBindingModel() {
    }

    @Length(min = 1, max = 100, message = "Recipient name must be between 1 and 100 characters!")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Size(min = 10, max = 1000, message = "Billing address length must be between 10 and 1000 characters!")
    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    @Pattern(regexp = "(\\+359|0)[0-9]{9}", message = "Enter valid phone number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Size(max = 1000, message = "Additional information length cannot be more than 1000 characters!")
    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }
}

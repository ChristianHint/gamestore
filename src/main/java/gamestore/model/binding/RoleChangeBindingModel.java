package gamestore.model.binding;

public class RoleChangeBindingModel {
    private String username;
    private String role;

    public RoleChangeBindingModel() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}

package gamestore.model.binding;


import gamestore.config.FieldMatch;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@FieldMatch(first = "newPassword", second = "confirmPassword", message = "Passwords must match.")
public class UserEditProfileBindingModel {
    private String username;
    private String newPassword;
    private String confirmPassword;
    private String email;

    public UserEditProfileBindingModel() {
    }

    @Length(min = 2, max = 20, message = "Username must be between 2 and 20 characters.")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Length(min = 2, max = 20, message = "Password must be between 2 and 20 characters.")
    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @NotEmpty
    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @Email(message = "Enter valid email address")
    @NotEmpty
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

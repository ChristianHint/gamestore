package gamestore.model.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "carts")
public class Cart extends BaseEntity{
    private User customer;
    private Game games;
    private BigDecimal totalPrice;
    private Order order;
    private boolean ordered;

    public Cart() {
    }

    @ManyToOne
    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    @ManyToOne
    public Game getGames() {
        return games;
    }

    public void setGames(Game games) {
        this.games = games;
    }

    @Column(name = "total_price")
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @ManyToOne
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Column(name = "ordered")
    public boolean isOrdered() {
        return ordered;
    }

    public void setOrdered(boolean ordered) {
        this.ordered = ordered;
    }
}

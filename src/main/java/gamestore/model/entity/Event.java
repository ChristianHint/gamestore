package gamestore.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "events")
public class Event extends BaseEntity {
    private String name;
    private String description;
    private LocalDateTime createDate;
    private LocalDateTime endedDate;
    private boolean isEventExpired;

    public Event() {
    }

    @Column(name = "name", nullable = false, unique = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", nullable = false, columnDefinition = "TEXT")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "created_on", nullable = false)
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    @Column(name = "ended_on", nullable = false)
    public LocalDateTime getEndedDate() {
        return endedDate;
    }

    public void setEndedDate(LocalDateTime endDate) {
        this.endedDate = endDate;
    }

    public boolean isEventExpired() {
        return isEventExpired;
    }

    public void setEventExpired(boolean eventExpired) {
        isEventExpired = eventExpired;
    }
}

package gamestore.model.service;

import java.math.BigDecimal;

public class CartServiceModel extends BaseServiceModel{
    private UserServiceModel customer;
    private GameServiceModel games;
    private BigDecimal totalPrice;
    private OrderServiceModel order;
    private boolean ordered;

    public CartServiceModel() {
    }

    public UserServiceModel getCustomer() {
        return customer;
    }

    public void setCustomer(UserServiceModel customer) {
        this.customer = customer;
    }

    public GameServiceModel getGames() {
        return games;
    }

    public void setGames(GameServiceModel games) {
        this.games = games;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        if(this.totalPrice == null){
            this.totalPrice = totalPrice;
        }
    }

    public OrderServiceModel getOrder() {
        return order;
    }

    public void setOrder(OrderServiceModel order) {
        this.order = order;
    }

    public boolean isOrdered() {
        return ordered;
    }

    public void setOrdered(boolean ordered) {
        this.ordered = ordered;
    }
}

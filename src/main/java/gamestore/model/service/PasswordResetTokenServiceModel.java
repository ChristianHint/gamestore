package gamestore.model.service;

import gamestore.model.entity.User;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class PasswordResetTokenServiceModel {
    private String token;
    private User user;
    private Date expiryDate;

    public PasswordResetTokenServiceModel(User user) {
        this.user = user;
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        dt = c.getTime();
        expiryDate = dt;
        token = UUID.randomUUID().toString();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}

package gamestore.model.view;

import gamestore.model.service.RoleServiceModel;

public class RoleChangeViewModel {
    private String id;
    private String username;
    private String email;
    private RoleServiceModel role;

    public RoleChangeViewModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public RoleServiceModel getRole() {
        return role;
    }

    public void setRole(RoleServiceModel role) {
        this.role = role;
    }
}

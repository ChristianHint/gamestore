package gamestore.repository;

import gamestore.model.entity.Cart;
import gamestore.model.entity.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository extends JpaRepository<Cart, String> {
    List<Cart> findByCustomer_IdAndOrderedIsFalse(String id);

    List<Cart> findAllByOrder_Id(String id);

    Cart findByGames(Game game);
}

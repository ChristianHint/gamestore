package gamestore.repository;

import gamestore.model.entity.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, String> {
    Page<Comment> findAllByGame_Id(String gameId, Pageable pageable);

    List<Comment> findAllByGame_Id(String id);

    Comment findByTextContent(String textContent);
}

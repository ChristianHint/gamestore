package gamestore.repository;

import gamestore.model.entity.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, String> {
    Event findByName(String name);

    Page<Event> findAllByEventExpiredIsFalse(Pageable pageable);

    List<Event> findAllByEventExpiredIsFalse();

}

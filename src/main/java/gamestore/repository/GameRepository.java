package gamestore.repository;

import gamestore.model.entity.Category;
import gamestore.model.entity.Game;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends JpaRepository<Game, String> {
    Game findByName(String name);

    List<Game> findAllByCategory(Category category);

    Page<Game> findAllByCategory(Category category, Pageable pageable);
}

package gamestore.service;

import gamestore.model.entity.Cart;
import gamestore.model.entity.Order;
import gamestore.model.service.CartServiceModel;
import gamestore.model.service.GameServiceModel;
import gamestore.model.service.UserServiceModel;

import java.util.List;

public interface CartService {
    void addToCart(UserServiceModel user, GameServiceModel game, CartServiceModel cart);

    List<Cart> myCart(UserServiceModel user);

    void deleteItemById(String id);

    void updateOrderId(Order order, Cart c);

    List<Cart> findAllCartItemsByOrderId(String id);

}

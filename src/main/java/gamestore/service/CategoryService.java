package gamestore.service;

import gamestore.model.entity.Category;
import gamestore.model.service.CategoryServiceModel;

import java.util.List;

public interface CategoryService {
    CategoryServiceModel findByName(String name);

    List<Category> findAllCategories();
}

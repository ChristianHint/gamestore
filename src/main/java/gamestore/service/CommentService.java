package gamestore.service;

import gamestore.model.entity.Comment;
import gamestore.model.service.CommentServiceModel;
import org.springframework.data.domain.Page;

public interface CommentService {
    void add(CommentServiceModel commentServiceModel);

    Page<Comment> findPaginated(int pageNum, int pageSize, String sortField, String sortDir, String gameId);

    CommentServiceModel findById(String id);

    void deleteCommentById(String id);

    void updateComment(CommentServiceModel comment, CommentServiceModel commentTemporary);
}

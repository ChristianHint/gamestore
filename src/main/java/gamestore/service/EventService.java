package gamestore.service;

import gamestore.model.entity.Event;
import gamestore.model.service.EventServiceModel;
import org.springframework.data.domain.Page;

import java.util.List;

public interface EventService {
    void addEvent(EventServiceModel eventServiceModel);

    List<Event> findAllEvents();

    EventServiceModel findById(String id);

    void deleteEventById(String id);

    void updateEvent(EventServiceModel event);

    boolean containsName(String name);

    Page<Event> findPaginated(int pageNum, int pageSize, String sortField, String sortDirection);

    Page<Event> findAllNonExpiredPaginated(int pageNum, int pageSize, String sortField, String sortDirection);

    List<Event> findAllEventsNotExpired();

    void updateExpiredEvent(Event e);
}

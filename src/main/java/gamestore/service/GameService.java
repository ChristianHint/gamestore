package gamestore.service;

import gamestore.model.entity.Category;
import gamestore.model.entity.Game;
import gamestore.model.service.GameServiceModel;
import org.springframework.data.domain.Page;

import java.util.List;

public interface GameService{
    Page<Game> findPaginated(int pageNum, int pageSize, String sortField, String sortDirection);

    List<Game> findGames();

    List<Game> findAllGamesByCategory(Category category);

    Page<Game> findPaginatedByCategory(int pageNum, int pageSize, String sortField, String sortDirection, Category category);

    boolean containsName(String name);

    void addGame(GameServiceModel gameServiceModel);

    GameServiceModel findById(String id);

    void deleteGameById(String id);

    void updateGame(GameServiceModel game);

    void onSaleGame(GameServiceModel game);

    void updateScore(GameServiceModel game);
}

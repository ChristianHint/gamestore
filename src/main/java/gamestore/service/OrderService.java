package gamestore.service;

import gamestore.model.entity.Cart;
import gamestore.model.entity.Order;
import gamestore.model.service.OrderServiceModel;
import org.springframework.data.domain.Page;

import java.util.List;

public interface OrderService {
    void addOrder(OrderServiceModel order, List<Cart> cartList);

    Page<Order> findPaginated(int pageNum, int pageSize, String sortField, String sortDir);

    List<Order> findOrders();

    OrderServiceModel findOrderById(String id);

    void confirmOrderById(String id);
}

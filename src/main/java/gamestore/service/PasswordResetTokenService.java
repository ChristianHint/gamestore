package gamestore.service;

import gamestore.model.entity.PasswordResetToken;

import java.util.List;

public interface PasswordResetTokenService {
    PasswordResetToken findByToken(String token);

    List<PasswordResetToken> findAllTokens();

    void deleteToken(PasswordResetToken p);
}

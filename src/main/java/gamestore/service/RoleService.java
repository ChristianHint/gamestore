package gamestore.service;

import gamestore.model.service.RoleServiceModel;

public interface RoleService {
    RoleServiceModel findByName(String name);
}

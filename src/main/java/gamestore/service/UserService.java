package gamestore.service;

import gamestore.model.entity.User;
import gamestore.model.service.UserServiceModel;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {
    UserServiceModel register(UserServiceModel userServiceModel);

    UserServiceModel findByUsername(String username);

    List<User> findUsers();

    List<User> findAdminUsers();

    void changeRole(String username, String role);

    UserServiceModel findById(String id);

    UserServiceModel update(UserServiceModel userServiceModel);

    Page<User> findPaginated(int pageNum, int pageSize, String sortField, String sortDirection);

    boolean containsName(String username);

    UserServiceModel findByEmail(String email);

    void updatePassword(UserServiceModel tokenUser, String password);
}

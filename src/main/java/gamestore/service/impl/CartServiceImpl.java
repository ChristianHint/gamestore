package gamestore.service.impl;

import gamestore.model.entity.Cart;
import gamestore.model.entity.Order;
import gamestore.model.service.CartServiceModel;
import gamestore.model.service.GameServiceModel;
import gamestore.model.service.UserServiceModel;
import gamestore.repository.CartRepository;
import gamestore.service.CartService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CartServiceImpl implements CartService {
    private final CartRepository cartRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public CartServiceImpl(CartRepository cartRepository, ModelMapper modelMapper) {
        this.cartRepository = cartRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public void addToCart(UserServiceModel user, GameServiceModel game, CartServiceModel cart) {

        cart.setGames(game);
        cart.setTotalPrice(game.getPrice());
        cart.setCustomer(user);
        cart.setOrdered(false);

        this.cartRepository.saveAndFlush(this.modelMapper.map(cart, Cart.class));
    }

    @Override
    public List<Cart> myCart(UserServiceModel user) {
        List<Cart> cart = new ArrayList<>(this.cartRepository.findByCustomer_IdAndOrderedIsFalse(user.getId()));

        cart.sort(Comparator.comparing(o -> o.getGames().getName()));

        return cart;
    }

    @Override
    public void deleteItemById(String id) {
        this.cartRepository.deleteById(id);
    }

    @Override
    public void updateOrderId(Order order, Cart c) {
        Optional<Cart> cart = this.cartRepository.findById(c.getId());
        Cart cartOut = this.modelMapper.map(cart, Cart.class);

        cartOut.setOrder(order);
        if(cart.isPresent()){
            cartOut.setCustomer(cart.get().getCustomer());
            cartOut.setGames(cart.get().getGames());
            cartOut.setTotalPrice(cart.get().getTotalPrice());
            cartOut.setId(cart.get().getId());
            cartOut.setOrdered(true);
        }

        this.modelMapper.map(this.cartRepository.saveAndFlush(cartOut), CartServiceModel.class);
    }

    @Override
    public List<Cart> findAllCartItemsByOrderId(String id) {
        return this.cartRepository.findAllByOrder_Id(id);
    }
}

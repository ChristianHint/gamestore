package gamestore.service.impl;

import gamestore.model.entity.Category;
import gamestore.model.service.CategoryServiceModel;
import gamestore.repository.CategoryRepository;
import gamestore.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, ModelMapper modelMapper) {
        this.categoryRepository = categoryRepository;
        this.modelMapper = modelMapper;
    }

    @PostConstruct
    public void init(){
        if(this.categoryRepository.count() == 0){
            Category fps = new Category("FPS");
            Category platformer = new Category("PLATFORMER");
            Category horror = new Category("HORROR");
            Category rpg = new Category("RPG");
            Category survival = new Category("SURVIVAL");
            Category simulation = new Category("SIMULATION");
            Category strategy = new Category("STRATEGY");
            Category racing = new Category("RACING");
            Category fighting = new Category("FIGHTING");
            Category battleRoyale = new Category("BATTLEROYALE");

            this.categoryRepository.save(fps);
            this.categoryRepository.save(platformer);
            this.categoryRepository.save(horror);
            this.categoryRepository.save(rpg);
            this.categoryRepository.save(survival);
            this.categoryRepository.save(simulation);
            this.categoryRepository.save(strategy);
            this.categoryRepository.save(racing);
            this.categoryRepository.save(fighting);
            this.categoryRepository.save(battleRoyale);

        }
    }

    @Override
    public CategoryServiceModel findByName(String name) {
        return this.categoryRepository.findByName(name)
                .map(category -> this.modelMapper
                        .map(category, CategoryServiceModel.class))
                .orElse(null);
    }

    @Override
    public List<Category> findAllCategories() {
        return this.categoryRepository.findAll();
    }
}

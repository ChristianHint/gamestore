package gamestore.service.impl;

import gamestore.model.entity.Comment;
import gamestore.model.service.CommentServiceModel;
import gamestore.model.service.GameServiceModel;
import gamestore.repository.CommentRepository;
import gamestore.service.CommentService;
import gamestore.service.GameService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;
    private final GameService gameService;
    private final ModelMapper modelMapper;

    public CommentServiceImpl(CommentRepository commentRepository, GameService gameService, ModelMapper modelMapper) {
        this.commentRepository = commentRepository;
        this.gameService = gameService;
        this.modelMapper = modelMapper;
    }

    @Override
    public void add(CommentServiceModel commentServiceModel) {
        this.commentRepository.saveAndFlush(this.modelMapper.map(commentServiceModel, Comment.class));
    }

    @Override
    public Page<Comment> findPaginated(int pageNum, int pageSize, String sortField, String sortDirection, String gameId) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNum - 1, pageSize, sort);

        return this.commentRepository.findAllByGame_Id(gameId, pageable);
    }

    @Override
    public CommentServiceModel findById(String id) {
        return this.commentRepository.findById(id).map(comment -> this.modelMapper.map(comment, CommentServiceModel.class)).orElse(null);
    }

    @Override
    public void deleteCommentById(String id) {
        CommentServiceModel comment = this.commentRepository.findById(id).map(commentInfo -> this.modelMapper.map(commentInfo, CommentServiceModel.class)).orElse(null);
        GameServiceModel game = this.gameService.findById(comment.getGame().getId());

        game.setScore(-comment.getScore());

        this.gameService.updateScore(game);
        this.commentRepository.deleteById(id);
    }

    @Override
    public void updateComment(CommentServiceModel comment, CommentServiceModel commentTemporary) {
        Comment commentUp = this.modelMapper.map(comment, Comment.class);

        GameServiceModel game = this.gameService.findById(comment.getGame().getId());
        game.setScore(-commentTemporary.getScore());

        this.gameService.updateScore(game);
        game.setScore(comment.getScore());

        this.gameService.updateScore(game);

        this.modelMapper.map(this.commentRepository.saveAndFlush(commentUp), CommentServiceModel.class);
    }
}

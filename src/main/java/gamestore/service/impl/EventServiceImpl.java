package gamestore.service.impl;

import gamestore.model.entity.Event;
import gamestore.model.service.EventServiceModel;
import gamestore.repository.EventRepository;
import gamestore.service.EventService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {
    private final EventRepository eventRepository;
    private final ModelMapper modelMapper;

    public EventServiceImpl(EventRepository eventRepository, ModelMapper modelMapper) {
        this.eventRepository = eventRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public void addEvent(EventServiceModel eventServiceModel) {
        Event event = this.modelMapper.map(eventServiceModel, Event.class);

        this.eventRepository.saveAndFlush(event);
    }

    @Override
    public List<Event> findAllEvents() {
        return new ArrayList<>(this.eventRepository.findAll());
    }

    @Override
    public List<Event> findAllEventsNotExpired() {
        return new ArrayList<>(this.eventRepository.findAllByEventExpiredIsFalse());
    }

    @Override
    public EventServiceModel findById(String id) {
        return this.eventRepository.findById(id).map(event -> this.modelMapper.map(event, EventServiceModel.class)).orElse(null);
    }

    @Override
    public void deleteEventById(String id) {
        this.eventRepository.deleteById(id);
    }

    @Override
    public void updateEvent(EventServiceModel event) {
        Event eventUp = this.modelMapper.map(event, Event.class);

        this.modelMapper.map(this.eventRepository.saveAndFlush(eventUp), EventServiceModel.class);
    }

    @Override
    public boolean containsName(String name) {
        Event event = this.eventRepository.findByName(name);

        return event != null;
    }

    @Override
    public Page<Event> findPaginated(int pageNum, int pageSize, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize, sort);

        return this.eventRepository.findAll(pageable);
    }

    @Override
    public Page<Event> findAllNonExpiredPaginated(int pageNum, int pageSize, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize, sort);

        return this.eventRepository.findAllByEventExpiredIsFalse(pageable);
    }

    @Override
    public void updateExpiredEvent(Event e) {
        this.modelMapper.map(this.eventRepository.saveAndFlush(e), EventServiceModel.class);
    }
}


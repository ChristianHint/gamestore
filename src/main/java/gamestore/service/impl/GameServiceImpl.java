package gamestore.service.impl;

import gamestore.model.entity.Category;
import gamestore.model.entity.Comment;
import gamestore.model.entity.Game;
import gamestore.model.service.CategoryServiceModel;
import gamestore.model.service.GameServiceModel;
import gamestore.repository.CommentRepository;
import gamestore.repository.GameRepository;
import gamestore.service.CategoryService;
import gamestore.service.GameService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GameServiceImpl implements GameService {
    private final GameRepository gameRepository;
    private final CommentRepository commentRepository;
    private final CategoryService categoryService;
    private final ModelMapper modelMapper;

    @Autowired
    public GameServiceImpl(GameRepository gameRepository, CommentRepository commentRepository, CategoryService categoryService, ModelMapper modelMapper) {
        this.gameRepository = gameRepository;
        this.commentRepository = commentRepository;
        this.categoryService = categoryService;
        this.modelMapper = modelMapper;
    }


    @Override
    public Page<Game> findPaginated(int pageNum, int pageSize, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize, sort);

        return this.gameRepository.findAll(pageable);
    }

    @Override
    public Page<Game> findPaginatedByCategory(int pageNum, int pageSize, String sortField, String sortDirection, Category category) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize, sort);

        return this.gameRepository.findAllByCategory(category, pageable);
    }

    @Override
    public List<Game> findGames() {
        return new ArrayList<>(this.gameRepository.findAll());
    }

    @Override
    public List<Game> findAllGamesByCategory(Category category) {
        return new ArrayList<>(this.gameRepository.findAllByCategory(category));
    }

    @Override
    public boolean containsName(String name) {
        Game game = this.gameRepository.findByName(name);

        return game != null;
    }

    @Override
    public void addGame(GameServiceModel gameServiceModel) {
        CategoryServiceModel category = this.categoryService.findByName(gameServiceModel.getCategory().getName());

        gameServiceModel.setCategory(category);
        gameServiceModel.setScore(0);
        gameServiceModel.setOnSale(false);
        Game game = this.modelMapper.map(gameServiceModel, Game.class);

        this.gameRepository.saveAndFlush(game);
    }

    @Override
    public GameServiceModel findById(String id) {
        return this.gameRepository.findById(id).map(game -> this.modelMapper.map(game, GameServiceModel.class)).orElse(null);
    }

    @Override
    public void deleteGameById(String id) {

        List<Comment> commentList = this.commentRepository.findAllByGame_Id(id);

        for (Comment c : commentList) {
            c.setAuthor(null);
            c.setGame(null);
            this.commentRepository.deleteById(c.getId());
        }

        this.gameRepository.deleteById(id);

    }

    @Override
    public void updateGame(GameServiceModel game) {
        Game gameUp = this.modelMapper.map(game, Game.class);

        this.modelMapper.map(this.gameRepository.saveAndFlush(gameUp), GameServiceModel.class);
    }

    @Override
    public void onSaleGame(GameServiceModel game) {
        Game gameSale = this.modelMapper.map(game, Game.class);

        gameSale.setOnSale(!game.isOnSale());

        this.modelMapper.map(this.gameRepository.saveAndFlush(gameSale), GameServiceModel.class);
    }

    @Override
    public void updateScore(GameServiceModel game) {
        Game gameUp = this.modelMapper.map(game, Game.class);

        this.modelMapper.map(this.gameRepository.saveAndFlush(gameUp), GameServiceModel.class);
    }


}

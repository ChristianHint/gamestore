package gamestore.service.impl;

import gamestore.model.entity.Cart;
import gamestore.model.entity.Order;
import gamestore.model.service.OrderServiceModel;
import gamestore.repository.CartRepository;
import gamestore.repository.OrderRepository;
import gamestore.service.CartService;
import gamestore.service.OrderService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final CartService cartService;
    private final CartRepository cartRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, CartService cartService, CartRepository cartRepository, ModelMapper modelMapper) {
        this.orderRepository = orderRepository;
        this.cartService = cartService;
        this.cartRepository = cartRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public void addOrder(OrderServiceModel order, List<Cart> cartList) {
        order.setCreateDateTime(LocalDateTime.now());
        Order orderOut = this.modelMapper.map(order, Order.class);

        this.orderRepository.saveAndFlush(orderOut);

        for (Cart c : cartList) {
            this.cartService.updateOrderId(orderOut, c);
        }

    }

    @Override
    public Page<Order> findPaginated(int pageNum, int pageSize, String sortField, String sortDir) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize, sort);

        return this.orderRepository.findAll(pageable);
    }

    @Override
    public List<Order> findOrders() {
        return new ArrayList<>(this.orderRepository.findAll());
    }

    @Override
    public OrderServiceModel findOrderById(String id) {
        return this.orderRepository.findById(id).map(orderInfo -> this.modelMapper.map(orderInfo, OrderServiceModel.class)).orElse(null);
    }

    @Override
    public void confirmOrderById(String id) {

        List<Cart> cart = this.cartService.findAllCartItemsByOrderId(id);

        for (Cart c : cart) {
            this.cartRepository.deleteById(c.getId());
        }

        this.orderRepository.deleteById(id);
    }
}

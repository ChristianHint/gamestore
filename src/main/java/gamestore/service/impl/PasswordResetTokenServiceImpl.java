package gamestore.service.impl;

import gamestore.model.entity.PasswordResetToken;
import gamestore.repository.PasswordResetTokenRepository;
import gamestore.service.PasswordResetTokenService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PasswordResetTokenServiceImpl implements PasswordResetTokenService {
    private final ModelMapper modelMapper;
    private final PasswordResetTokenRepository passwordResetTokenRepository;

    public PasswordResetTokenServiceImpl(ModelMapper modelMapper, PasswordResetTokenRepository passwordResetTokenRepository) {
        this.modelMapper = modelMapper;
        this.passwordResetTokenRepository = passwordResetTokenRepository;
    }


    @Override
    public PasswordResetToken findByToken(String token) {
        return this.passwordResetTokenRepository.findByToken(token);
    }

    @Override
    public List<PasswordResetToken> findAllTokens() {
        return new ArrayList<>(this.passwordResetTokenRepository.findAll());
    }

    @Override
    public void deleteToken(PasswordResetToken p) {
        this.passwordResetTokenRepository.deleteById(p.getId());
    }
}

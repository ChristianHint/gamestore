package gamestore.service.impl;

import gamestore.model.entity.Role;
import gamestore.model.entity.User;
import gamestore.model.service.RoleServiceModel;
import gamestore.model.service.UserServiceModel;
import gamestore.repository.UserRepository;
import gamestore.service.RoleService;
import gamestore.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleService roleService;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleService roleService, ModelMapper modelMapper, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleService = roleService;
        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserServiceModel findByUsername(String username) {
        return this.userRepository.findByUsername(username).map(user -> this.modelMapper.map(user, UserServiceModel.class)).orElse(null);
    }

    @Override
    public UserServiceModel register(UserServiceModel userServiceModel) {

        userServiceModel.setRole(this.roleService.findByName(this.userRepository.count() == 0 ? "ADMIN" : "USER"));

        userServiceModel.setEnabled(true);
        User user = this.modelMapper.map(userServiceModel, User.class);
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return this.modelMapper.map(this.userRepository.saveAndFlush(user), UserServiceModel.class);
    }

    @Override
    public List<User> findUsers() {
        return new ArrayList<>(this.userRepository.findAll());
    }

    @Override
    public List<User> findAdminUsers() {
        RoleServiceModel role = this.roleService.findByName("ADMIN");

        return new ArrayList<>(this.userRepository.findAllByRoleId(role.getId()));
    }

    @Override
    public void changeRole(String username, String role) {
        User user = this.userRepository.findByUsername(username).orElse(null);

        if (!user.getRole().getName().equals(role)) {
            if (role.equals("remove")) {
                this.userRepository.deleteById(user.getId());
            } else {
                Role roleEntity = this.modelMapper.map(this.roleService.findByName(role), Role.class);

                user.setRole(roleEntity);

                this.userRepository.saveAndFlush(user);
            }
        }
    }

    @Override
    public UserServiceModel findById(String id) {
        return this.userRepository.findById(id).map(user -> this.modelMapper.map(user, UserServiceModel.class)).orElse(null);
    }

    @Override
    public UserServiceModel update(UserServiceModel userServiceModel) {
        User user = this.modelMapper.map(userServiceModel, User.class);
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return this.modelMapper.map(this.userRepository.saveAndFlush(user), UserServiceModel.class);
    }

    @Override
    public Page<User> findPaginated(int pageNum, int pageSize, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize, sort);

        return this.userRepository.findAll(pageable);
    }

    @Override
    public boolean containsName(String username) {
        User user = this.userRepository.findByUsername(username).orElse(null);

        return user != null;
    }

    @Override
    public UserServiceModel findByEmail(String email) {
        return this.userRepository.findByEmailIgnoreCase(email).map(user -> this.modelMapper.map(user, UserServiceModel.class)).orElse(null);
    }

    @Override
    public void updatePassword(UserServiceModel tokenUser, String password) {
        User user = this.modelMapper.map(tokenUser, User.class);
        user.setPassword(passwordEncoder.encode(password));

        this.modelMapper.map(this.userRepository.saveAndFlush(user), UserServiceModel.class);
    }

}

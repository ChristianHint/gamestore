package gamestore.web;

import gamestore.model.Details.MyUserDetails;
import gamestore.model.binding.OrderAddBindingModel;
import gamestore.model.entity.Cart;
import gamestore.model.service.CartServiceModel;
import gamestore.model.service.GameServiceModel;
import gamestore.model.service.OrderServiceModel;
import gamestore.model.service.UserServiceModel;
import gamestore.service.CartService;
import gamestore.service.GameService;
import gamestore.service.OrderService;
import gamestore.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@Controller
@RequestMapping("/cart")
public class CartController {

    private final CartService cartService;
    private final GameService gameService;
    private final UserService userService;
    private final OrderService orderService;
    private final ModelMapper modelMapper;

    @Autowired
    public CartController(CartService cartService, GameService gameService, UserService userService, OrderService orderService, ModelMapper modelMapper) {
        this.cartService = cartService;
        this.gameService = gameService;
        this.userService = userService;
        this.orderService = orderService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/add")
    public String addToCart(HttpServletRequest request, @ModelAttribute CartServiceModel cart, @RequestParam("id") String id, @AuthenticationPrincipal MyUserDetails myUserDetails){
        GameServiceModel game = this.gameService.findById(id);
        UserServiceModel user = this.userService.findByUsername(myUserDetails.getUsername());

        this.cartService.addToCart(user, game, cart);

        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping("/view")
    public String viewCart(@AuthenticationPrincipal MyUserDetails myUserDetails, Model model){
        UserServiceModel user = this.userService.findByUsername(myUserDetails.getUsername());

        List<Cart> cartList = this.cartService.myCart(user);

        model.addAttribute("cartItems", cartList);

        BigDecimal totalPrice = new BigDecimal(0);

        for (Cart c : cartList) {
            totalPrice = totalPrice.add(c.getTotalPrice());
        }

        model.addAttribute("totalPrice", totalPrice);

        return "cart";
    }

    @GetMapping("/remove")
    public String removeItem(@RequestParam("id") String id, HttpServletRequest request){
        this.cartService.deleteItemById(id);

        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping("/checkout")
    public ModelAndView cartCheckout(@AuthenticationPrincipal MyUserDetails myUserDetails, Model model, ModelAndView modelAndView){
        UserServiceModel user = this.userService.findByUsername(myUserDetails.getUsername());

        List<Cart> cartList = this.cartService.myCart(user);

        BigDecimal totalPrice = new BigDecimal(0);

        for (Cart c : cartList) {
            totalPrice = totalPrice.add(c.getTotalPrice());
        }

        if (!model.containsAttribute("orderAddBindingModel")) {
            model.addAttribute("orderAddBindingModel", new OrderAddBindingModel());
            model.addAttribute("cartItems", cartList);
            model.addAttribute("totalPrice", totalPrice);
        }

        modelAndView.setViewName("checkout");

        return modelAndView;
    }

    @PostMapping("/checkout")
    public String checkoutConfirm(@Valid @ModelAttribute("orderAddBindingModel") OrderAddBindingModel orderAddBindingModel,
                                  BindingResult bindingResult, RedirectAttributes redirectAttributes, @AuthenticationPrincipal MyUserDetails myUserDetails){

        UserServiceModel user = this.userService.findByUsername(myUserDetails.getUsername());

        List<Cart> cartList = this.cartService.myCart(user);

        BigDecimal totalPrice = new BigDecimal(0);

        for (Cart c : cartList) {
            totalPrice = totalPrice.add(c.getTotalPrice());
        }

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("orderAddBindingModel", orderAddBindingModel);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.orderAddBindingModel", bindingResult);
            redirectAttributes.addFlashAttribute("cartItems", cartList);
            redirectAttributes.addFlashAttribute("totalPrice", totalPrice);
            return "redirect:checkout";
        }else{
            this.orderService.addOrder(this.modelMapper.map(orderAddBindingModel, OrderServiceModel.class), cartList);
        }

        return "index";
    }
}

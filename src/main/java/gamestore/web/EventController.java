package gamestore.web;

import gamestore.model.binding.EventAddBindingModel;
import gamestore.model.binding.EventChangeBindingModel;
import gamestore.model.entity.Event;
import gamestore.model.service.EventServiceModel;
import gamestore.service.EventService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/sidebar/events")
public class EventController {

    private final EventService eventService;
    private final ModelMapper modelMapper;

    @Autowired
    public EventController(EventService eventService, ModelMapper modelMapper) {
        this.eventService = eventService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/add")
    public String eventAdd(Model model) {
        if (!model.containsAttribute("eventAddBindingModel")) {
            model.addAttribute("eventAddBindingModel", new EventAddBindingModel());
        }

        if(model.containsAttribute("hasErrors")){
            model.addAttribute("hasErrors", true);
        }

        return "events-add";
    }

    @PostMapping("/add")
    public String eventAddConfirm(@Valid @ModelAttribute("eventAddBindingModel") EventAddBindingModel eventAddBindingModel,
                                  BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors() || eventAddBindingModel.getCreateDate().isAfter(eventAddBindingModel.getEndedDate()) || this.eventService.containsName(eventAddBindingModel.getName())) {
            redirectAttributes.addFlashAttribute("eventAddBindingModel", eventAddBindingModel);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.eventAddBindingModel", bindingResult);
            redirectAttributes.addFlashAttribute("hasErrors", true);
            return "redirect:add";
        } else {
            this.eventService.addEvent(this.modelMapper.map(eventAddBindingModel, EventServiceModel.class));

            return "redirect:/sidebar/events";
        }
    }

    @GetMapping("/change")
    public String eventChange(Model model, @RequestParam("id") String id, HttpSession httpSession) {
        if (!model.containsAttribute("eventChangeBindingModel")) {
            model.addAttribute("eventChangeBindingModel", new EventChangeBindingModel());
            EventServiceModel event = this.eventService.findById(id);
            model.addAttribute("event", event);
            httpSession.setAttribute("eventId", event.getId());
        }

        if(model.containsAttribute("hasErrors")){
            model.addAttribute("hasErrors", true);
        }

        return "events-change";
    }

    @PostMapping("/change")
    public String eventChangeConfirm(@Valid @ModelAttribute("eventChangeBindingModel") EventChangeBindingModel eventChangeBindingModel,
                                     BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpSession httpSession, Model model) {

        EventServiceModel event = this.eventService.findById(httpSession.getAttribute("eventId").toString());

        if (bindingResult.hasErrors() || (this.eventService.containsName(eventChangeBindingModel.getName()) && !event.getName().equals(eventChangeBindingModel.getName()))) {
            redirectAttributes.addFlashAttribute("eventChangeBindingModel", eventChangeBindingModel);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.eventChangeBindingModel", bindingResult);
            redirectAttributes.addFlashAttribute("event", event);
            redirectAttributes.addFlashAttribute("hasErrors", true);
            return "redirect:change/?id=" + event.getId();
        } else {
            event.setName(eventChangeBindingModel.getName());
            event.setDescription(eventChangeBindingModel.getDescription());
            event.setCreateDate(eventChangeBindingModel.getCreateDate());
            event.setEndedDate(eventChangeBindingModel.getEndedDate());

            httpSession.removeAttribute("eventId");
            this.eventService.updateEvent(event);

            return "redirect:/sidebar/events";
        }

    }

    @GetMapping("/change/delete")
    public String eventDelete(@RequestParam("id") String id) {
        this.eventService.deleteEventById(id);

        return "redirect:/sidebar/events";
    }

    @GetMapping("/view")
    public ModelAndView getEventView(Model model){

        return getEventsPages(1, "name", "asc", model);
    }

    @GetMapping("/view/page/{pageNum}")
    public ModelAndView getEventsPages( @PathVariable (value = "pageNum") int pageNum,@RequestParam("sortField") String sortField
            ,@RequestParam("sortDir") String sortDir ,Model model){
        int pageSize = 10;
        ModelAndView modelAndView = new ModelAndView();
        Page<Event> page = eventService.findPaginated(pageNum, pageSize, sortField, sortDir);

        modelAndView.addObject("events", this.eventService.findAllEvents());
        modelAndView.setViewName("events-list");

        List<Event> listUsers = page.getContent();

        model.addAttribute("currentPage", pageNum);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDirection", sortDir);
        model.addAttribute("reverseSortDirection", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("listEvents", listUsers);
        return modelAndView;
    }

    @GetMapping("/viewer")
    public ModelAndView profile(ModelAndView modelAndView, @RequestParam("id") String id) {
        EventServiceModel event = this.eventService.findById(id);
        modelAndView.addObject("eventInfo", event);
        modelAndView.setViewName("events-viewer");

        return modelAndView;
    }
}

package gamestore.web;

import gamestore.model.Details.MyUserDetails;
import gamestore.model.binding.*;
import gamestore.model.entity.Comment;
import gamestore.model.entity.Game;
import gamestore.model.service.CategoryServiceModel;
import gamestore.model.service.CommentServiceModel;
import gamestore.model.service.GameServiceModel;
import gamestore.model.service.UserServiceModel;
import gamestore.service.CategoryService;
import gamestore.service.CommentService;
import gamestore.service.GameService;
import gamestore.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
public class GameController {
    private final GameService gameService;
    private final CategoryService categoryService;
    private final CommentService commentService;
    private final UserService userService;
    private final ModelMapper modelMapper;

    @Autowired
    public GameController(GameService gameService, CategoryService categoryService, CommentService commentService, UserService userService, ModelMapper modelMapper) {
        this.gameService = gameService;
        this.categoryService = categoryService;
        this.commentService = commentService;
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/games/add")
    public String gamesAdd(Model model){
        if (!model.containsAttribute("gameAddBindingModel")) {
            model.addAttribute("gameAddBindingModel", new GameAddBindingModel());
        }

        model.addAttribute("categories", this.categoryService.findAllCategories());

        if(model.containsAttribute("hasErrors")){
            model.addAttribute("hasErrors", true);
        }

        return "games-add";
    }

    @PostMapping("/games/add")
    public String gameAddConfirm(@Valid @ModelAttribute("gameAddBindingModel") GameAddBindingModel gameAddBindingModel, BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpSession httpSession){
        if(bindingResult.hasErrors() || this.gameService.containsName(gameAddBindingModel.getName())){
            redirectAttributes.addFlashAttribute("gameAddBindingModel", gameAddBindingModel);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.gameAddBindingModel", bindingResult);
            redirectAttributes.addFlashAttribute("hasErrors", true);

            return "redirect:add";
        }else{
            this.gameService.addGame(this.modelMapper.map(gameAddBindingModel, GameServiceModel.class));

            return "redirect:/sidebar/games";
        }
    }

    @GetMapping("/games/view")
    public ModelAndView gamesView(Model model, @RequestParam("id") String id, @AuthenticationPrincipal MyUserDetails myUserDetails){
       // model.addAttribute("game", this.gameService.findById(id));
       // model.addAttribute("comments", this.gameService.findById(id).getComments());
       // modelAndView.setViewName("shop-description");

        return getGamesViewPages(1, "score", "asc", id ,model, myUserDetails);
    }

    @GetMapping("/games/view/page/{pageNum}")
    public ModelAndView getGamesViewPages(@PathVariable (value = "pageNum") int pageNum, @RequestParam("sortField") String sortField
            ,@RequestParam("sortDir") String sortDir, @RequestParam("id") String id, Model model, @AuthenticationPrincipal MyUserDetails myUserDetails){
        int pageSize = 10;
        ModelAndView modelAndView = new ModelAndView();

        boolean hasAlreadyCommented = false;

        GameServiceModel game = this.gameService.findById(id);
        model.addAttribute("game", game);
        Page<Comment> page = this.commentService.findPaginated(pageNum, pageSize, sortField, sortDir, game.getId());

        List<Comment> listComments = page.getContent();

        UserServiceModel user = this.userService.findByUsername(myUserDetails.getUsername());

        for (Comment c: game.getComments()) {
            if (c.getAuthor().getId().equals(user.getId())) {
                hasAlreadyCommented = true;
                break;
            }
        }

        modelAndView.addObject("comments", game.getComments());
        modelAndView.setViewName("shop-description");

        model.addAttribute("currentPage", pageNum);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("userId", user.getId());
        model.addAttribute("hasAlreadyCommented", hasAlreadyCommented);
        model.addAttribute("gameId", game.getId());
        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDirection", sortDir);
        model.addAttribute("reverseSortDirection", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("listComments", listComments);
        return modelAndView;
    }

    @GetMapping("/games/view-list")
    public ModelAndView gamesListView(Model model){

        return getGamesPages(1, "name", "asc", model);
    }

    @GetMapping("/games/view-list/page/{pageNum}")
    public ModelAndView getGamesPages(@PathVariable (value = "pageNum") int pageNum, @RequestParam("sortField") String sortField
            ,@RequestParam("sortDir") String sortDir, Model model){
        int pageSize = 15;
        ModelAndView modelAndView = new ModelAndView();
        Page<Game> page = gameService.findPaginated(pageNum, pageSize, sortField, sortDir);

        List<Game> listGames = page.getContent();
        modelAndView.addObject("games", this.gameService.findGames());
        modelAndView.setViewName("game-list");

        model.addAttribute("currentPage", pageNum);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDirection", sortDir);
        model.addAttribute("reverseSortDirection", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("listGames", listGames);
        return modelAndView;
    }

    @GetMapping("/games/change")
    public String gameChange(Model model, @RequestParam("id") String id, HttpSession httpSession) {
        if (!model.containsAttribute("gameChangeBindingModel")) {
            model.addAttribute("gameChangeBindingModel", new GameChangeBindingModel());
            GameServiceModel game = this.gameService.findById(id);
            model.addAttribute("game", game);
            httpSession.setAttribute("gameId", game.getId());
        }

        model.addAttribute("categories", this.categoryService.findAllCategories());

        if(model.containsAttribute("hasErrors")){
            model.addAttribute("hasErrors", true);
        }

        return "games-change";
    }

    @PostMapping("/games/change")
    public String gameChangeConfirm(@Valid @ModelAttribute("gameChangeBindingModel") GameChangeBindingModel gameChangeBindingModel,
                                     BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpSession httpSession) {

        GameServiceModel game = this.gameService.findById(httpSession.getAttribute("gameId").toString());

        if (bindingResult.hasErrors() || (this.gameService.containsName(gameChangeBindingModel.getName()) && !game.getName().equals(gameChangeBindingModel.getName()))) {
            redirectAttributes.addFlashAttribute("gameChangeBindingModel", gameChangeBindingModel);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.gameChangeBindingModel", bindingResult);
            redirectAttributes.addFlashAttribute("game", game);
            redirectAttributes.addFlashAttribute("hasErrors", true);
            return "redirect:change/?id=" + game.getId();
        } else {
            game.setName(gameChangeBindingModel.getName());
            game.setDescription(gameChangeBindingModel.getDescription());
            game.setPlatform(gameChangeBindingModel.getPlatform());
            game.setRequirements(gameChangeBindingModel.getRequirements());
            CategoryServiceModel category = this.categoryService.findByName(gameChangeBindingModel.getCategory().getName());
            game.setCategory(category);
            game.setPrice(gameChangeBindingModel.getPrice());
            game.setOnSale(false);

            httpSession.removeAttribute("gameId");

            this.gameService.updateGame(game);

            return "redirect:/sidebar/games";
        }

    }

    @GetMapping("/games/change/delete")
    public String eventDelete(@RequestParam("id") String id) {
        this.gameService.deleteGameById(id);

        return "redirect:/games/view-list";
    }

    @GetMapping("/games/sale")
    public String gameSale(Model model, @RequestParam("id") String id, HttpSession httpSession) {
        if (!model.containsAttribute("gameSaleBindingModel")) {
            model.addAttribute("gameSaleBindingModel", new GameSaleBindingModel());
            GameServiceModel game = this.gameService.findById(id);
            model.addAttribute("game", game);
            httpSession.setAttribute("gameId", game.getId());
        }

        return "on-sale";
    }

    @PostMapping("/games/sale")
    public String saleConfirm(@Valid @ModelAttribute("gameSaleBindingModel") GameSaleBindingModel gameSaleBindingModel,
                              BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpSession httpSession){

        GameServiceModel game = this.gameService.findById(httpSession.getAttribute("gameId").toString());

        if(bindingResult.hasErrors()){
            redirectAttributes.addFlashAttribute("gameSaleBindingModel", gameSaleBindingModel);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.gameSaleBindingModel", bindingResult);
            redirectAttributes.addFlashAttribute("game", game);
            return "redirect:sale/?id=" + game.getId();
        }else{
            game.setSalePrice(game.getPrice());
            game.setPrice(gameSaleBindingModel.getSalePrice());
            httpSession.removeAttribute("gameId");

            this.gameService.onSaleGame(game);

            return "redirect:/sidebar/games";
        }
    }

    @GetMapping("/games/comment")
    public ModelAndView getCommentForm(ModelAndView modelAndView, Model model, @RequestParam("id") String id, HttpSession httpSession){
        if (!model.containsAttribute("gameAddCommentBindingModel")) {
            model.addAttribute("gameAddCommentBindingModel", new GameAddCommentBindingModel());
            GameServiceModel game = this.gameService.findById(id);
            httpSession.setAttribute("gameId", game.getId());
        }

        model.addAttribute("game", this.gameService.findById(id));

        if(model.containsAttribute("hasErrors")){
            model.addAttribute("hasErrors", true);
        }

        modelAndView.setViewName("games-comment");

        return modelAndView;
    }

    @PostMapping("/games/comment")
    public String confirmCommentForm(@Valid @ModelAttribute("gameAddCommentBindingModel")GameAddCommentBindingModel gameAddCommentBindingModel, BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpSession httpSession
    ,@AuthenticationPrincipal MyUserDetails myUserDetails){
        GameServiceModel game = this.gameService.findById(httpSession.getAttribute("gameId").toString());
        UserServiceModel user = this.userService.findByUsername(myUserDetails.getUsername());

        if(bindingResult.hasErrors()){
            redirectAttributes.addFlashAttribute("gameAddCommentBindingModel", gameAddCommentBindingModel);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.gameAddCommentBindingModel", bindingResult);
            redirectAttributes.addFlashAttribute("game", game);
            redirectAttributes.addFlashAttribute("hasErrors", true);
            return "redirect:comment/?id=" + game.getId();
        }else{
            CommentServiceModel commentServiceModel = this.modelMapper.map(gameAddCommentBindingModel, CommentServiceModel.class);
            commentServiceModel.setGame(game);
            commentServiceModel.setScore(gameAddCommentBindingModel.getScore());
            commentServiceModel.setTextContent(gameAddCommentBindingModel.getTextContent());
            commentServiceModel.setAuthor(user);
            game.setScore(gameAddCommentBindingModel.getScore());

            this.gameService.updateScore(game);
            this.commentService.add(commentServiceModel);

            return "redirect:view/?id=" + game.getId();
        }

    }

    @GetMapping("/games/comment/change")
    public ModelAndView getCommentChangeForm(ModelAndView modelAndView, Model model, @RequestParam("id") String id, HttpSession httpSession){
        if (!model.containsAttribute("gameCommentChangeBindingModel")) {
            model.addAttribute("gameCommentChangeBindingModel", new GameCommentChangeBindingModel());
            CommentServiceModel comment = this.commentService.findById(id);
            httpSession.setAttribute("commentId", comment.getId());
        }

        model.addAttribute("game", this.gameService.findById(this.commentService.findById(id).getGame().getId()));
        model.addAttribute("comment", this.commentService.findById(id));

        if(model.containsAttribute("hasErrors")){
            model.addAttribute("hasErrors", true);
        }

        modelAndView.setViewName("games-comment-change");

        return modelAndView;
    }

    @PostMapping("/games/comment/change")
    public String commentChangeConfirm(@Valid @ModelAttribute("gameCommentChangeBindingModel") GameCommentChangeBindingModel gameCommentChangeBindingModel,
                                    BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpSession httpSession) {

        CommentServiceModel comment = this.commentService.findById(httpSession.getAttribute("commentId").toString());
        CommentServiceModel commentTemporary = this.commentService.findById(httpSession.getAttribute("commentId").toString());
        GameServiceModel game = this.gameService.findById(comment.getGame().getId());

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("gameCommentChangeBindingModel", gameCommentChangeBindingModel);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.gameCommentChangeBindingModel", bindingResult);
            redirectAttributes.addFlashAttribute("comment", comment);
            redirectAttributes.addFlashAttribute("game", game);
            redirectAttributes.addFlashAttribute("hasErrors", true);
            return "redirect:change/?id=" + comment.getId();
        } else {
            comment.setTextContent(gameCommentChangeBindingModel.getTextContent());
            comment.setScore(gameCommentChangeBindingModel.getScore());

            httpSession.removeAttribute("commentId");

            this.commentService.updateComment(comment, commentTemporary);

            return "redirect:/sidebar/games";
        }

    }

    @GetMapping("/games/comment/change/delete")
    public String commentDelete(@RequestParam("id") String id) {
        this.commentService.deleteCommentById(id);

        return "redirect:/sidebar/games";
    }

}

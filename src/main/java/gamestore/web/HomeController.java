package gamestore.web;

import gamestore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class HomeController {

    @Autowired
    public HomeController() {

    }

    @GetMapping("/login")
    public String loginPage() {

        return "login";
    }


    @GetMapping("/")
    public String homePage() {

        return "index";
    }

}

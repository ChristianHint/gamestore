package gamestore.web;

import gamestore.model.entity.Cart;
import gamestore.model.service.OrderServiceModel;
import gamestore.service.OrderService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;

@Controller
@RequestMapping("/order")
public class OrderController {
    private final OrderService orderService;
    private final ModelMapper modelMapper;

    public OrderController(OrderService orderService, ModelMapper modelMapper) {
        this.orderService = orderService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/view")
    public String viewOrder(Model model, @RequestParam("id") String id){
        OrderServiceModel order = this.orderService.findOrderById(id);

        List<Cart> cartList = order.getCart();

        BigDecimal totalPrice = new BigDecimal(0);

        for (Cart c : cartList) {
            totalPrice = totalPrice.add(c.getTotalPrice());
        }

        model.addAttribute("totalPrice", totalPrice);
        model.addAttribute("order", order);

        return "order-view";
    }

    @GetMapping("/confirm")
    public String confirmOrder(@RequestParam("id") String id){
        this.orderService.confirmOrderById(id);

        return "redirect:/sidebar/orders";
    }
}

package gamestore.web.RestControllers;

import com.google.gson.Gson;
import gamestore.model.entity.Event;
import gamestore.model.view.CalendarView;
import gamestore.service.EventService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CalendarRestController {

    private final EventService eventService;

    public CalendarRestController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping(value = "/calendar", produces = MediaType.APPLICATION_JSON_VALUE)
    public String homeAjax() {
        List<Event> events = this.eventService.findAllEventsNotExpired();

        List<CalendarView> calendarViewList = new ArrayList<>();

        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;

        for (Event event : events) {
            CalendarView calendarView = new CalendarView();
            calendarView.setId(event.getId());
            calendarView.setTitle(event.getName());
            calendarView.setDescription(event.getDescription());

            String formattedStart = event.getCreateDate().format(formatter);
            String formattedEnd = event.getEndedDate().format(formatter);

            calendarView.setStart(formattedStart);
            calendarView.setEnd(formattedEnd);

            calendarViewList.add(calendarView);
        }

        String json = new Gson().toJson(calendarViewList);

        System.out.println(json);

        return json;
    }

}

package gamestore.web;

import gamestore.model.binding.RoleChangeBindingModel;
import gamestore.model.view.RoleChangeViewModel;
import gamestore.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/roles")
public class RoleController {

    private final UserService userService;
    private final ModelMapper modelMapper;

    public RoleController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/change")
    public String change(Model model, @RequestParam("id") String id) {
        model.addAttribute("user", this.modelMapper.map(this.userService.findById(id), RoleChangeViewModel.class));

        return "role-change";
    }

    @PostMapping("/change")
    public String changeConfirm(@ModelAttribute("roleChangeBindingModel") RoleChangeBindingModel roleChangeBindingModel){
        this.userService.changeRole(roleChangeBindingModel.getUsername(), roleChangeBindingModel.getRole());

        return "redirect:/sidebar/userlist";
    }
}

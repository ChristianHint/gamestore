package gamestore.web;

import gamestore.model.Details.MyUserDetails;
import gamestore.model.entity.Category;
import gamestore.model.entity.Game;
import gamestore.model.entity.Order;
import gamestore.model.entity.User;
import gamestore.model.service.CategoryServiceModel;
import gamestore.model.service.UserServiceModel;
import gamestore.service.*;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class SidebarController {

    private final EventService eventService;
    private final UserService userService;
    private final GameService gameService;
    private final CategoryService categoryService;
    private final OrderService orderService;
    private final ModelMapper modelMapper;

    public SidebarController(EventService eventService, UserService userService, GameService gameService, CategoryService categoryService, OrderService orderService, ModelMapper modelMapper) {
        this.eventService = eventService;
        this.userService = userService;
        this.gameService = gameService;
        this.categoryService = categoryService;
        this.orderService = orderService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/sidebar/userlist")
    public ModelAndView userList(Model model, @AuthenticationPrincipal MyUserDetails myUserDetails){

        return getUserPages(1, "username", "asc", model, myUserDetails);
    }

    @GetMapping("sidebar/userlist/page/{pageNum}")
    public ModelAndView getUserPages(@PathVariable (value = "pageNum") int pageNum, @RequestParam("sortField") String sortField
            , @RequestParam("sortDir") String sortDir, Model model, @AuthenticationPrincipal MyUserDetails myUserDetails){
        int pageSize = 10;
        ModelAndView modelAndView = new ModelAndView();
        Page<User> page = userService.findPaginated(pageNum, pageSize, sortField, sortDir);

        UserServiceModel user = this.userService.findByUsername(myUserDetails.getUsername());

        List<User> listUsers = page.getContent();
        modelAndView.addObject("users", this.userService.findUsers());
        modelAndView.addObject("admins", this.userService.findAdminUsers());
        modelAndView.setViewName("users-list");

        model.addAttribute("currentPage", pageNum);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("userInfo", user);

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDirection", sortDir);
        model.addAttribute("reverseSortDirection", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("listUsers", listUsers);
        return modelAndView;
    }


    @GetMapping("/sidebar/events")
    public ModelAndView eventList(ModelAndView modelAndView){
        modelAndView.addObject("events", this.eventService.findAllEventsNotExpired());
        modelAndView.setViewName("events");

        return modelAndView;
    }

    @GetMapping("/sidebar/about")
    public String aboutPage(){
        return "about";
    }

    @GetMapping("/sidebar/games")
    public ModelAndView gamesShop(Model model){

        return getGamesPages(1, "name", "asc", "all", model);
    }

    @GetMapping("sidebar/games/page/{pageNum}")
    public ModelAndView getGamesPages(@PathVariable (value = "pageNum") int pageNum, @RequestParam("sortField") String sortField
            ,@RequestParam("sortDir") String sortDir,@RequestParam("category") String category, Model model){
        int pageSize = 15;
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("categories", this.categoryService.findAllCategories());

        if(category.equals("all")){
            Page<Game> page = gameService.findPaginated(pageNum, pageSize, sortField, sortDir);

            List<Game> listGames = page.getContent();
            modelAndView.addObject("games", this.gameService.findGames());

            model.addAttribute("totalPages", page.getTotalPages());
            model.addAttribute("totalItems", page.getTotalElements());

            model.addAttribute("listGames", listGames);
        }else{
            Category categoryObject = this.modelMapper.map(this.categoryService.findByName(category), Category.class);

            Page<Game> page = gameService.findPaginatedByCategory(pageNum, pageSize, sortField, sortDir, categoryObject);

            List<Game> listGames = page.getContent();
            modelAndView.addObject("games", this.gameService.findAllGamesByCategory(categoryObject));

            model.addAttribute("totalPages", page.getTotalPages());
            model.addAttribute("totalItems", page.getTotalElements());

            model.addAttribute("listGames", listGames);
        }

        modelAndView.setViewName("shop");

        model.addAttribute("category", category);
        model.addAttribute("currentPage", pageNum);

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDirection", sortDir);
        model.addAttribute("reverseSortDirection", sortDir.equals("asc") ? "desc" : "asc");

        return modelAndView;
    }

    @GetMapping("/sidebar/orders")
    public ModelAndView ordersView(Model model){

        return getOrdersPages(1, "createDateTime", "asc", model);
    }

    @GetMapping("sidebar/orders/page/{pageNum}")
    public ModelAndView getOrdersPages(@PathVariable (value = "pageNum") int pageNum, @RequestParam("sortField") String sortField
            ,@RequestParam("sortDir") String sortDir, Model model){

        int pageSize = 15;
        ModelAndView modelAndView = new ModelAndView();
        Page<Order> page = this.orderService.findPaginated(pageNum, pageSize, sortField, sortDir);

        List<Order> listOrders = page.getContent();
        modelAndView.addObject("orders", this.orderService.findOrders());
        modelAndView.setViewName("order-list");

        model.addAttribute("currentPage", pageNum);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDirection", sortDir);
        model.addAttribute("reverseSortDirection", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("listOrders", listOrders);

        return modelAndView;
    }
}

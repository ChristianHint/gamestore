package gamestore.web;

import gamestore.model.Details.MyUserDetails;
import gamestore.model.binding.UserEditProfileBindingModel;
import gamestore.model.binding.UserRegisterBindingModel;
import gamestore.model.entity.PasswordResetToken;
import gamestore.model.entity.User;
import gamestore.model.service.PasswordResetTokenServiceModel;
import gamestore.model.service.UserServiceModel;
import gamestore.model.view.UserProfileViewModel;
import gamestore.repository.PasswordResetTokenRepository;
import gamestore.service.UserService;
import gamestore.service.impl.EmailSenderService;
import org.modelmapper.ModelMapper;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final ModelMapper modelMapper;

    private final EmailSenderService emailSenderService;
    private final PasswordResetTokenRepository passwordResetTokenRepository;

    public UserController(UserService userService, ModelMapper modelMapper, EmailSenderService emailSenderService, PasswordResetTokenRepository passwordResetTokenRepository) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.emailSenderService = emailSenderService;
        this.passwordResetTokenRepository = passwordResetTokenRepository;
    }

    @GetMapping("/register")
    public String register(Model model) {

        if (!model.containsAttribute("userRegisterBindingModel")) {
            model.addAttribute("userRegisterBindingModel", new UserRegisterBindingModel());
        }

        return "register";
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("userRegisterBindingModel") UserRegisterBindingModel userRegisterBindingModel,
                           BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors() || !userRegisterBindingModel.getPassword().equals(userRegisterBindingModel.getConfirmPassword())) {

            redirectAttributes.addFlashAttribute("userRegisterBindingModel", userRegisterBindingModel);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.userRegisterBindingModel", bindingResult);
            return "redirect:register";
        } else {
            this.userService.register(this.modelMapper.map(userRegisterBindingModel, UserServiceModel.class));
            return "redirect:login";
        }
    }

    @GetMapping("/profile")
    public ModelAndView profile(ModelAndView modelAndView, Model model, @RequestParam("name") String name, @AuthenticationPrincipal MyUserDetails myUserDetails) {
        model.addAttribute("user", this.modelMapper.map(this.userService.findByUsername(name), UserProfileViewModel.class));
        UserServiceModel user = this.userService.findByUsername(myUserDetails.getUsername());
        modelAndView.addObject("userInfo", user);
        modelAndView.setViewName("profile");

        return modelAndView;
    }

    @GetMapping("/forgot-password")
    public ModelAndView displayResetPassword(ModelAndView modelAndView, User user) {
        modelAndView.addObject("user", user);
        modelAndView.setViewName("forgot-password");
        return modelAndView;
    }

    @PostMapping("/forgot-password")
    public ModelAndView forgotUserPassword(ModelAndView modelAndView, User user) {
        User existingUser = this.modelMapper.map(this.userService.findByEmail(user.getEmail()), User.class);

        if (existingUser != null) {
            // Create token
            PasswordResetTokenServiceModel confirmationToken = new PasswordResetTokenServiceModel(existingUser);

            // Save it
            this.passwordResetTokenRepository.saveAndFlush(this.modelMapper.map(confirmationToken, PasswordResetToken.class));

            // Create the email
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(user.getEmail());
            mailMessage.setSubject("Complete Password Reset!");
            mailMessage.setFrom("test-email@gmail.com");
            mailMessage.setText("To complete the password reset process, please click here: "
                    + "http://localhost:8080/users/confirm-reset?token="+confirmationToken.getToken());

            // Send the email

            emailSenderService.sendEmail(mailMessage);

            modelAndView.addObject("message", "Request to reset password received. Check your inbox for the reset link.");
            modelAndView.setViewName("successForgotPassword");

        } else {
            modelAndView.addObject("message", "This email address does not exist!");
            modelAndView.setViewName("error");
        }
        return modelAndView;
    }

    @RequestMapping(value="/confirm-reset", method= {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView validateResetToken(ModelAndView modelAndView, @RequestParam("token")String confirmationToken) {
        PasswordResetToken token = this.passwordResetTokenRepository.findByToken(confirmationToken);

        if (token != null) {
            UserServiceModel user = this.userService.findByEmail(token.getUser().getEmail());
            modelAndView.addObject("user", user);
            modelAndView.addObject("email", user.getEmail());
            modelAndView.setViewName("resetPassword");
        } else {
            modelAndView.addObject("message", "The link is invalid or broken!");
            modelAndView.setViewName("error");
        }
        return modelAndView;
    }

    @PostMapping("/reset-password")
    public ModelAndView resetUserPassword(ModelAndView modelAndView, User user) {
        if (user.getEmail() != null) {
            // Use email to find user
            UserServiceModel tokenUser = this.userService.findByEmail(user.getEmail());
            this.userService.updatePassword(tokenUser, user.getPassword());
            modelAndView.addObject("message", "Password successfully reset. You can now log in with the new credentials.");
            modelAndView.setViewName("successResetPassword");
        } else {
            modelAndView.addObject("message","The link is invalid or broken!");
            modelAndView.setViewName("error");
        }
        return modelAndView;
    }

    @GetMapping("/edit-profile")
    public ModelAndView editProfile(Model model, ModelAndView modelAndView, @AuthenticationPrincipal MyUserDetails myUserDetails ){
        if (!model.containsAttribute("userEditProfileBindingModel")) {
            model.addAttribute("userEditProfileBindingModel", new UserEditProfileBindingModel());
            model.addAttribute("notFound", false);
        }

        UserServiceModel user = this.userService.findByUsername(myUserDetails.getUsername());
        modelAndView.addObject("userInfo", user);

        if(model.containsAttribute("hasErrors")){
            model.addAttribute("hasErrors", true);
        }

        modelAndView.setViewName("editprofile");

        return modelAndView;
    }

    @PostMapping("/edit-profile")
    public ModelAndView editProfileConfirm(@Valid @ModelAttribute("userEditProfileBindingModel") UserEditProfileBindingModel userEditProfileBindingModel,
                                           BindingResult bindingResult, RedirectAttributes redirectAttributes, ModelAndView modelAndView, @AuthenticationPrincipal MyUserDetails myUserDetails){

        UserServiceModel user = this.userService.findByUsername(myUserDetails.getUsername());

        if (bindingResult.hasErrors() || (this.userService.containsName(userEditProfileBindingModel.getUsername()) && !user.getUsername().equals(userEditProfileBindingModel.getUsername()))) {
            redirectAttributes.addFlashAttribute("userEditProfileBindingModel", userEditProfileBindingModel);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.userEditProfileBindingModel", bindingResult);
            modelAndView.setViewName("redirect:edit-profile");
        }else{

            if(user == null || !userEditProfileBindingModel.getNewPassword().equals(userEditProfileBindingModel.getConfirmPassword())){
                redirectAttributes.addFlashAttribute("userEditProfileBindingModel", userEditProfileBindingModel);
                redirectAttributes.addFlashAttribute("notFound", true);
                redirectAttributes.addFlashAttribute("hasErrors", true);
                modelAndView.setViewName("redirect:edit-profile");
            }else{
                user.setUsername(userEditProfileBindingModel.getUsername());
                user.setEmail(userEditProfileBindingModel.getEmail());

                user.setPassword(userEditProfileBindingModel.getNewPassword());

                this.userService.update(user);

                modelAndView.setViewName("redirect:/");
            }
        }

        return modelAndView;
    }
}

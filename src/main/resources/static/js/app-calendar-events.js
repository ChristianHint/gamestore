// sample calendar events data
	'use strict'
	var curYear = moment().format('YYYY');
	var curMonth = moment().format('MM');

// Calendar Event Source
	var sptCalendarEvents = {
		id: 1,
		backgroundColor: 'rgba(107, 62, 158, 0.5)',
		borderColor: 'rgb(107, 62, 158)',
		events: []
	};
// Birthday Events Source
	var sptBirthdayEvents = {
		id: 2,
		backgroundColor: 'rgba(246, 107, 78,0.5)',
		borderColor: 'rgb(246, 107, 78)',
		events: []
	};
	var sptHolidayEvents = {
		id: 3,
		backgroundColor: 'rgba(78, 204, 72,0.5)',
		borderColor: 'rgb(78, 204, 72)',
		events: []
	};
	var sptOtherEvents = {
		id: 4,
		backgroundColor: 'rgba(69, 170, 242,0.5) ',
		borderColor: 'rgb(69, 170, 242) ',
		events: []
	};


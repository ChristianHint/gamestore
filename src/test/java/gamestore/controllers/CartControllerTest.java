package gamestore.controllers;

import gamestore.model.binding.OrderAddBindingModel;
import gamestore.model.binding.UserEditProfileBindingModel;
import gamestore.model.binding.UserRegisterBindingModel;
import gamestore.model.entity.Cart;
import gamestore.model.entity.Game;
import gamestore.model.entity.Order;
import gamestore.model.entity.User;
import gamestore.repository.CartRepository;
import gamestore.repository.GameRepository;
import gamestore.repository.OrderRepository;
import gamestore.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration
@WebAppConfiguration
@AutoConfigureMockMvc
public class CartControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private CartRepository cartRepository;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    @WithUserDetails("Dod")
    @Test
    public void cartAddGetSuccess() throws Exception {
        Game game = this.gameRepository.findByName("Game");

        mockMvc.perform(get("/cart/add").with(csrf()).param("id", game.getId())).andExpect(status().isFound());
    }

    @WithUserDetails("Dod")
    @Test
    public void cartViewGetSuccess() throws Exception {

        mockMvc.perform(get("/cart/view")).andExpect(status().isOk());
    }

    @WithUserDetails("Dod")
    @Test
    public void cartRemoveGetSuccess() throws Exception {
        Game game = this.gameRepository.findByName("Game");
        Cart cart = this.cartRepository.findByGames(game);

        mockMvc.perform(get("/cart/remove").with(csrf()).param("id", cart.getId())).andExpect(status().isFound());
    }

    @WithUserDetails("Dod")
    @Test
    public void cartCheckoutGetSuccess() throws Exception {

        mockMvc.perform(get("/cart/checkout")).andExpect(status().isOk());
    }

    @WithUserDetails("Dod")
    @Test
    public void cartCheckoutPostSuccess() throws Exception {
        OrderAddBindingModel orderAddBindingModel = new OrderAddBindingModel();
        orderAddBindingModel.setName("Order");
        orderAddBindingModel.setAdditionalInformation("Add Information");
        orderAddBindingModel.setBillingAddress("Test Address");
        orderAddBindingModel.setPhoneNumber("0895704271");

        mockMvc.perform(post("/cart/checkout").with(csrf()).flashAttr("orderAddBindingModel", orderAddBindingModel)).andExpect(status().isOk());
    }



}

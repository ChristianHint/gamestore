package gamestore.controllers;

import gamestore.model.binding.EventAddBindingModel;
import gamestore.model.binding.EventChangeBindingModel;
import gamestore.model.binding.UserEditProfileBindingModel;
import gamestore.model.binding.UserRegisterBindingModel;
import gamestore.model.entity.Event;
import gamestore.model.entity.User;
import gamestore.repository.EventRepository;
import gamestore.service.EventService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration
@WebAppConfiguration
@AutoConfigureMockMvc
public class EventControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private EventRepository eventRepository;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    @WithUserDetails("Dod")
    @Test
    public void eventsAddGetSuccess() throws Exception {
        String url = "/sidebar/events/add";

        mockMvc.perform(get(url)).andExpect(status().isOk());
    }

    @WithUserDetails("Dod")
    @Test
    public void eventsAddPostSuccess() throws Exception {
        String url = "/sidebar/events/add";
        EventAddBindingModel eventAddBindingModel = new EventAddBindingModel();
        eventAddBindingModel.setName("Name");
        eventAddBindingModel.setDescription("description");
        eventAddBindingModel.setCreateDate(LocalDateTime.now().plusDays(1));
        eventAddBindingModel.setEndedDate(LocalDateTime.now().plusDays(7));

        mockMvc.perform(post(url).with(csrf()).flashAttr("eventAddBindingModel", eventAddBindingModel)).andExpect(redirectedUrl("/sidebar/events")).andExpect(status().isFound());
    }

    @WithUserDetails("Dod")
    @Test
    public void eventsChangeGetSuccess() throws Exception {
        String url = "/sidebar/events/change";
        Event event = this.eventRepository.findByName("Name");

        mockMvc.perform(get(url).param("id", event.getId())).andExpect(status().isOk());
    }

    @WithUserDetails("Dod")
    @Test
    public void eventsChangePostSuccess() throws Exception {
        String url = "/sidebar/events/change";
        EventChangeBindingModel eventChangeBindingModel = new EventChangeBindingModel();

        Event event = this.eventRepository.findByName("Name");

        eventChangeBindingModel.setName("Name");
        eventChangeBindingModel.setDescription("description");
        eventChangeBindingModel.setCreateDate(LocalDateTime.now().plusDays(1));
        eventChangeBindingModel.setEndedDate(LocalDateTime.now().plusDays(7));

        mockMvc.perform(post(url).with(csrf()).flashAttr("eventChangeBindingModel", eventChangeBindingModel).sessionAttr("eventId", event.getId())).andExpect(redirectedUrl("/sidebar/events")).andExpect(status().isFound());
    }

    @WithUserDetails("Dod")
    @Test
    public void eventsDeleteGetSuccess() throws Exception {
        String url = "/sidebar/events/change/delete";
        Event event = this.eventRepository.findByName("Name");

        mockMvc.perform(get(url).param("id", event.getId())).andExpect(redirectedUrl("/sidebar/events")).andExpect(status().isFound());
    }



    @WithUserDetails("Dod")
    @Test
    public void eventsViewGetSuccess() throws Exception {
        String url = "/sidebar/events/view";

        mockMvc.perform(get(url)).andExpect(status().isOk());
    }



    @WithUserDetails("Dod")
    @Test
    public void eventsViewerGetSuccess() throws Exception {
        String url = "/sidebar/events/viewer";

        mockMvc.perform(get(url).param("id", "ed35d07c-d31a-4ea0-9a93-ea9fd77c0bec")).andExpect(status().isOk());
    }
}

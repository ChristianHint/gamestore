package gamestore.controllers;

import gamestore.model.binding.*;
import gamestore.model.entity.*;
import gamestore.model.service.CategoryServiceModel;
import gamestore.repository.CategoryRepository;
import gamestore.repository.CommentRepository;
import gamestore.repository.GameRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration
@WebAppConfiguration
@AutoConfigureMockMvc
public class GameControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    @WithUserDetails("Dod")
    @Test
    public void gamesAddGetSuccess() throws Exception{

        mockMvc.perform(get("/games/add")).andExpect(status().isOk());
    }

    @WithUserDetails("Dod")
    @Test
    public void gamesAddPostSuccess() throws Exception {
        String url = "/games/add";
        GameAddBindingModel gameAddBindingModel = new GameAddBindingModel();

        Category category = this.categoryRepository.findByName("FPS").map(category1 -> this.modelMapper.map(category1, Category.class)).orElse(null);

        gameAddBindingModel.setName("Game");
        gameAddBindingModel.setCategory(category);
        gameAddBindingModel.setDescription("description");
        gameAddBindingModel.setPlatform("test platform");
        gameAddBindingModel.setPrice(BigDecimal.TEN);
        gameAddBindingModel.setRequirements("test requirements");

        mockMvc.perform(post(url).with(csrf()).flashAttr("gameAddBindingModel", gameAddBindingModel)).andExpect(redirectedUrl("/sidebar/games")).andExpect(status().isFound());
    }

    @WithUserDetails("Dod")
    @Test
    public void gamesViewGetSuccess() throws Exception{
        Game game = this.gameRepository.findByName("Game");

        mockMvc.perform(get("/games/view").param("id", game.getId())).andExpect(status().isOk());
    }



    @WithUserDetails("Dod")
    @Test
    public void gamesViewListGetSuccess() throws Exception{

        mockMvc.perform(get("/games/view-list")).andExpect(status().isOk());
    }



    @WithUserDetails("Dod")
    @Test
    public void gamesChangeGetSuccess() throws Exception{
        Game game = this.gameRepository.findByName("Game");

        mockMvc.perform(get("/games/change").param("id", game.getId())).andExpect(status().isOk());
    }

    @WithUserDetails("Dod")
    @Test
    public void gamesChangePostSuccess() throws Exception {
        String url = "/games/change";
        GameChangeBindingModel gameChangeBindingModel = new GameChangeBindingModel();
        Game game = this.gameRepository.findByName("Game");

        Category category = this.categoryRepository.findByName("FPS").map(category1 -> this.modelMapper.map(category1, Category.class)).orElse(null);

        gameChangeBindingModel.setName("Game");
        gameChangeBindingModel.setCategory(category);
        gameChangeBindingModel.setDescription("description");
        gameChangeBindingModel.setPlatform("test platform");
        gameChangeBindingModel.setPrice(BigDecimal.TEN);
        gameChangeBindingModel.setRequirements("test requirements");

        mockMvc.perform(post(url).with(csrf()).flashAttr("gameChangeBindingModel", gameChangeBindingModel).sessionAttr("gameId", game.getId())).andExpect(redirectedUrl("/sidebar/games")).andExpect(status().isFound());
    }

    @WithUserDetails("Dod")
    @Test
    public void gamesChangeDeleteGetSuccess() throws Exception{
        Game game = this.gameRepository.findByName("Game");

        mockMvc.perform(get("/games/change/delete").param("id", game.getId())).andExpect(redirectedUrl("/games/view-list")).andExpect(status().isFound());
    }



    @WithUserDetails("Dod")
    @Test
    public void gamesSaleGetSuccess() throws Exception{
        Game game = this.gameRepository.findByName("Game");

        mockMvc.perform(get("/games/sale").param("id", game.getId())).andExpect(status().isOk());
    }

    @WithUserDetails("Dod")
    @Test
    public void gamesSalePostSuccess() throws Exception {
        String url = "/games/sale";
        GameSaleBindingModel gameSaleBindingModel = new GameSaleBindingModel();
        Game game = this.gameRepository.findByName("Game");

        gameSaleBindingModel.setSalePrice(BigDecimal.ONE);

        mockMvc.perform(post(url).with(csrf()).flashAttr("gameSaleBindingModel", gameSaleBindingModel).sessionAttr("gameId", game.getId())).andExpect(redirectedUrl("/sidebar/games")).andExpect(status().isFound());
    }

    @WithUserDetails("Dod")
    @Test
    public void gamesCommentsAddGetSuccess() throws Exception{
        Game game = this.gameRepository.findByName("Game");

        mockMvc.perform(get("/games/comment").param("id", game.getId())).andExpect(status().isOk());
    }

    @WithUserDetails("Dod")
    @Test
    public void gamesCommentAddPostSuccess() throws Exception {
        String url = "/games/comment";
        Game game = this.gameRepository.findByName("Game");
        GameAddCommentBindingModel gameAddCommentBindingModel = new GameAddCommentBindingModel();

        gameAddCommentBindingModel.setTextContent("TextTextText");
        gameAddCommentBindingModel.setScore(5);

        mockMvc.perform(post(url).with(csrf()).flashAttr("gameAddCommentBindingModel", gameAddCommentBindingModel).sessionAttr("gameId", game.getId())).andExpect(status().isFound());
    }

    @WithUserDetails("Dod")
    @Test
    public void gamesCommentsChangeGetSuccess() throws Exception{
        Comment comment = this.commentRepository.findByTextContent("TextTextText");

        mockMvc.perform(get("/games/comment/change").param("id", comment.getId())).andExpect(status().isOk());
    }

    @WithUserDetails("Dod")
    @Test
    public void gamesCommentsChangePostSuccess() throws Exception {
        String url = "/games/comment/change";
        GameCommentChangeBindingModel gameCommentChangeBindingModel = new GameCommentChangeBindingModel();
        Comment comment = this.commentRepository.findByTextContent("TextTextText");

        gameCommentChangeBindingModel.setTextContent("TextTextText");
        gameCommentChangeBindingModel.setScore(5);

        mockMvc.perform(post(url).with(csrf()).flashAttr("gameCommentChangeBindingModel", gameCommentChangeBindingModel).sessionAttr("commentId", comment.getId())).andExpect(redirectedUrl("/sidebar/games")).andExpect(status().isFound());
    }

    @WithUserDetails("Dod")
    @Test
    public void gamesCommentsChangeDeleteGetSuccess() throws Exception{
        Comment comment = this.commentRepository.findByTextContent("TextTextText");

        mockMvc.perform(get("/games/comment/change/delete").param("id", comment.getId())).andExpect(redirectedUrl("/sidebar/games")).andExpect(status().isFound());
    }
}

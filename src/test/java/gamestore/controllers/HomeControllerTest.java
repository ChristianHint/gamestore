package gamestore.controllers;

import gamestore.web.HomeController;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HomeControllerTest {

    @Test
    public void homeTest(){
        HomeController homeController = new HomeController();

        String response = homeController.homePage();
        assertEquals("index", response);
    }

    @Test
    public void loginTest(){
        HomeController homeController = new HomeController();

        String response = homeController.loginPage();
        assertEquals("login", response);
    }
}

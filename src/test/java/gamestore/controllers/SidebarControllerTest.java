package gamestore.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration
@WebAppConfiguration
@AutoConfigureMockMvc
public class SidebarControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    @WithUserDetails("Dod")
    @Test
    public void sidebarUserListGetSuccess() throws Exception{
        mockMvc.perform(get("/sidebar/userlist").with(csrf())).andExpect(status().isOk());
    }



    @WithUserDetails("Dod")
    @Test
    public void sidebarEventsGetSuccess() throws Exception{
        mockMvc.perform(get("/sidebar/events").with(csrf())).andExpect(status().isOk());
    }




    @WithUserDetails("Dod")
    @Test
    public void sidebarAboutGetSuccess() throws Exception{
        mockMvc.perform(get("/sidebar/about").with(csrf())).andExpect(status().isOk());
    }



    @WithUserDetails("Dod")
    @Test
    public void sidebarGamesGetSuccess() throws Exception{
        mockMvc.perform(get("/sidebar/games").with(csrf())).andExpect(status().isOk());
    }



    @WithUserDetails("Dod")
    @Test
    public void sidebarRolesGetSuccess() throws Exception{
        mockMvc.perform(get("/sidebar/orders").with(csrf())).andExpect(status().isOk());
    }


}

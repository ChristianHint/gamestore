package gamestore.controllers;

import gamestore.model.binding.UserEditProfileBindingModel;
import gamestore.model.binding.UserRegisterBindingModel;
import gamestore.model.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration
@WebAppConfiguration
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    @Test
    public void loginGetAvailableForAll() throws Exception{
        mockMvc.perform(get("/login")).andExpect(status().isOk());
    }

    @Test
    public void loginPostSuccessAndRedirectAndLogoutSuccess() throws Exception {
        String adminUsername = "Dod";
        mockMvc.perform(post("/login").with(csrf()).param("username", "Dod").param("password", "123")).andExpect(status().isFound()).andExpect(redirectedUrl("/")).andExpect(authenticated().withUsername(adminUsername));

        mockMvc
                .perform(logout())
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/login?logout"));
    }

    @Test
    public void registerGetSuccess() throws Exception{
        mockMvc.perform(get("/users/register")).andExpect(status().isOk());
    }

    @Test
    public void registerPostSuccessAndRedirect() throws Exception {
        String url = "/users/register";
        UserRegisterBindingModel userRegisterBindingModel = new UserRegisterBindingModel();
        userRegisterBindingModel.setUsername("Test User");
        userRegisterBindingModel.setEmail("test@email.com");
        userRegisterBindingModel.setPassword("123");
        userRegisterBindingModel.setConfirmPassword("123");

        mockMvc.perform(post(url).with(csrf())
                .flashAttr("userRegisterBindingModel", userRegisterBindingModel))
                .andExpect(redirectedUrl("login")).andExpect(status().isFound());
    }

    @Test
    public void forgotPasswordGetSuccess() throws Exception {
        String url = "/users/forgot-password";

        mockMvc.perform(get(url)).andExpect(status().isOk());
    }

    @Test
    public void confirmResetPasswordGetSuccess() throws Exception {
        String url = "/users/confirm-reset";

        mockMvc.perform(get(url).with(csrf()).param("token", "testToken")).andExpect(status().isOk());
    }

    @Test
    public void resetPasswordPostSuccess() throws Exception {
        String url = "/users/reset-password";

        mockMvc.perform(post(url).with(csrf()).flashAttr("user", new User())).andExpect(status().isOk());
    }


    @WithUserDetails("Dod")
    @Test
    public void profileGetSuccess() throws Exception {
        String url = "/users/profile";

        mockMvc.perform(get(url).with(csrf()).param("name", "Dod")).andExpect(status().isOk());
    }

    @WithUserDetails("Dod")
    @Test
    public void editProfileGetSuccess() throws Exception {
        String url = "/users/edit-profile";

        mockMvc.perform(get(url).with(csrf())).andExpect(status().isOk());
    }

    @WithUserDetails("Dod")
    @Test
    public void editProfilePostAndRedirect() throws Exception {
        String url = "/users/edit-profile";

        UserEditProfileBindingModel userEditProfileBindingModel = new UserEditProfileBindingModel();
        userEditProfileBindingModel.setUsername("Dod");
        userEditProfileBindingModel.setEmail("test@abv.bg");
        userEditProfileBindingModel.setNewPassword("123");
        userEditProfileBindingModel.setConfirmPassword("123");

        mockMvc.perform(post(url).with(csrf()).flashAttr("userEditProfileBindingModel", userEditProfileBindingModel)).andExpect(redirectedUrl("/")).andExpect(status().isFound());
    }

}
